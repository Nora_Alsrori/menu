﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

using UnityEngine.UI;


public class SplashScreen : MonoBehaviour {

    public Image splashImage;
    public string LoadLevel;
    IEnumerator Start()
    { splashImage.canvasRenderer.SetAlpha(0.0f);
    fadein();
    yield return new WaitForSeconds(2.5f);
    fadeout();
    yield return new WaitForSeconds(2.5f);
    SceneManager.LoadScene(LoadLevel);
    }


    void fadein()
    { splashImage.CrossFadeAlpha(1.0f,1.5f,false ); }
    void fadeout()
    { splashImage.CrossFadeAlpha(0.0f,2.5f,false); }
		
	
}
